# ESPNow gateway

Gateway for receiving and sending messages via ESPNow. It uses HTTP requests to receive messages and send commands to espnow peers.

Requires Micropython with support for ESPNow: https://github.com/micropython/micropython/pull/6515

Web server part based on https://gitlab.com/superfly/dawndoor.

## How to use:

  * Edit src/data/config.json with proper network configuration.
  * Copy content of src/ to root of ESP32.
  * Reset.
  * Open in web browser http://ip.of.esp.32/

