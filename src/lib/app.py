import gc
import uasyncio as asyncio
import utime as time
import re

from esp import espnow
from network import AP_IF
from primitives.queue import Queue

from lib import data
from lib.web import WebApp, jsonify
from lib.wifi import connect, get_ip, is_connected

T_OFFSET = 946681200+3600

DEFAULT_GROUP = 'default'

config = data.Config('/data/config.json')
print("CONFIG: ", config)

webapp = WebApp()
espn = espnow.ESPNow()

toesp_que = Queue()
send_ret_que = Queue()
fromesp_que = {}

grp_peers_map = {}

stats = {}


@webapp.route('/', method='GET')
def index(request, response):
    """
    The main page
    """
    gc.collect()
    yield from webapp.sendfile(response, '/templates/index.html')


@webapp.route('/config/network', method='GET')
def get_network(request, response):
    """
    Return the WiFi config
    """
    network_config = config['network'] if 'network' in config else {}
    network_config['ifconfig'] = get_ip()
    if 'password' in network_config:
        network_config['password'] = '**********'
    gc.collect()
    yield from jsonify(response, network_config)


@webapp.route('/config/network', method='POST')
def save_network(request, response):
    """
    Save the network config
    """
    yield from request.read_form_data()
    updated_config = config['network'] if 'network' in config else {}
    for key in ['essid', 'password', 'ntp']:
        if key in request.form:
            updated_config[key] = request.form[key]

    valid = 'essid' in updated_config and 'password' in updated_config
    if valid:
        config['network'] = updated_config
        config.save()
        gc.collect()
        # Now try to connect to the WiFi network
        connect()
        gc.collect()
        updated_config['ifconfig'] = get_ip()
        updated_config['status'] = 'OK'
        if 'password' in updated_config:
            updated_config['password'] = '**********'
    else:
        updated_config = {'status': 'error',
                          'message': 'Param essid or password missing'}
    gc.collect()
    yield from jsonify(response, updated_config)


@webapp.route('/config/peers', method='GET')
def get_peers(request, response):
    """
    Return all peers
    """
    peers_s = get_espnow_peers()
    gc.collect()
    yield from jsonify(response, peers_s)


@webapp.route('/config/peers/add', method='POST')
def add_peers(request, response):
    """
    Add a new peer(s)
    """
    yield from request.read_form_data()
    peers_s = {'status': 'OK'}
    new_peers = get_peers_from_form(request.form)
    for peer in new_peers:
        peer = data.convert_mac_to_b(peer)
        try:
            espn.add_peer(peer, ifidx=AP_IF)
        except Exception as err:
            peers_s['status'] = 'error'
            peers_s['message'] = 'Add peer {} failed with {}'.format(peer, err)
    peers_s.update(get_espnow_peers())
    config['peers'] = peers_s['peers']
    config.save()
    gc.collect()
    yield from jsonify(response, peers_s)


@webapp.route('/config/peers/del', method='POST')
def del_peers(request, response):
    """
    Delete unused peer(s)
    """
    yield from request.read_form_data()
    old_peers = get_peers_from_form(request.form)

    peers_s = {'status': 'OK'}
    for peer in old_peers:
        peer = data.convert_mac_to_b(peer)
        try:
            espn.del_peer(peer)
        except Exception as err:
            peers_s['status'] = 'error'
            peers_s['message'] = 'Del peer {} failed with {}'.format(peer, err)

    peers_s.update(get_espnow_peers())
    config['peers'] = peers_s['peers']
    config.save()
    gc.collect()
    yield from jsonify(response, peers_s)


@webapp.route('/config/peers/set', method='POST')
def set_peers(request, response):
    """
    Replace current peer(s)
    """
    yield from request.read_form_data()
    new_peers = get_peers_from_form(request.form)
    for peer in espn.get_peers():
        try:
            espn.del_peer(peer[0])
        except OSError:
            pass
    for peer in new_peers:
        peer = data.convert_mac_to_b(peer)
        espn.add_peer(peer, ifidx=AP_IF)

    peers_s = get_espnow_peers()
    config['peers'] = peers_s['peers']
    config.save()
    peers_s['status'] = 'OK'
    gc.collect()
    yield from jsonify(response, peers_s)


@webapp.route('/config/groups', method='GET')
def get_groups(request, response):
    """
    Return all groups settings
    """
    groups = {'groups': config.get('groups', [])}
    groups['status'] = 'OK'
    gc.collect()
    yield from jsonify(response, groups)


@webapp.route(re.compile('/config/groups/(add|del)/?$'), method='POST')
def manage_groups(request, response):
    """
    manage groups
    """
    global grp_peers_map
    yield from request.read_form_data()

    group = request.form.get('group', None)
    peers = get_peers_from_form(request.form)

    status = {'status': 'OK'}
    if group and len(peers) > 0:
        if 'groups' not in config:
            config['groups'] = {}
        if 'add' in request.path:
            if group in config['groups']:
                for peer in peers:
                    if peer not in config['groups'][group]:
                        config['groups'][group].append(peer)
            else:
                config['groups'][group] = peers
                fromesp_que[group] = data.SavedQueue('/data/{}/'.format(group))
        elif 'del' in request.path:
            if group in config['groups']:
                for peer in peers:
                    if peer in config['groups'][group]:
                        config['groups'][group].remove(peer)
            else:
                status['status'] = 'ERROR'
                status['message'] = 'Group does not exits'
        config.save()
        grp_peers_map = mk_grp_map(config.get('groups', {}))
        if group in config['groups']:
            status[group] = config['groups'][group]
    else:
        status['status'] = 'ERROR'
        status['message'] = 'Missing parameter group or peers'
    yield from jsonify(response, status)


@webapp.route('/status', method='GET')
def send_stats(request, response):
    current = time.localtime()
    for i in stats:
        if 'time' in stats[i]:
            stats[i]['status'] = 'OK'
            if stats[i]['time'] < time.time()+T_OFFSET-1200:
                stats[i]['status'] = 'WARNING'
            if stats[i]['time'] < time.time()+T_OFFSET-2400:
                stats[i]['status'] = 'FAIL'
        else:
            stats[i]['status'] = 'UNKNOWN'
    current_time = {'year': current[0],
                    'month': current[1],
                    'mday': current[2],
                    'hour': current[3],
                    'minute': current[4],
                    'second': current[5],
                    'weekday': current[6],
                    'yearday': current[7]}
    dt = {'status': 'OK',
          'data': {'last_resp': stats,
                   'msg_queue_status': fromesp_que_status(),
                   'filesystem': data.get_fs_stats(),
                   'time': {'utime': time.time()+T_OFFSET,
                            'localtime': current_time}}}
    gc.collect()
    yield from jsonify(response, dt)


@webapp.route(re.compile('/data/[^/]+/?$'), method='GET')
def get_data(request, response):
    """
    Send collected data. Response may contain many items,
    so response is build from each data line instead of use jsonify
    """
    s_path = request.path.split('/')
    print('GET_DATA s_path:', s_path)
    grp = s_path[2] if len(s_path) > 2 else DEFAULT_GROUP
    yield from response.awrite("""HTTP/1.0 200 NA\r\n""")
    yield from response.awrite("""Content-Type: application/json\r\n\r\n""")
    yield from response.awrite("""{ "data": [\r\n""")
    delim = '  '
    for msg in fromesp_que[grp].get_all():
        if msg:
            yield from response.awrite(delim)
            yield from response.awrite(msg)
            delim = ',\r\n  '
    yield from response.awrite("""\r\n  ]\r\n}\r\n""")
    gc.collect()


@webapp.route('/sendto', method='POST')
def send_to_espnow(request, response):
    """
    Send command to remote
    """
    yield from request.read_form_data()
    peers = get_peers_from_form(request.form)
    command = None
    if 'command' in request.form:
        command = request.form['command']

    answer = {}
    if (peers == []) or (command is None):
        answer = {'status': 'error',
                  'message': 'peers or command not found in request'}
    else:
        for peer in peers:
            await toesp_que.put((peer, command))
        answer = {'status': 'qeued',
                  'message': 'All commands qeued'}

    gc.collect()
    yield from jsonify(response, answer)


@webapp.route('/sendto/results', method='GET')
def send_results_of_sent(request, response):
    """
    Send saved results of sent commands
    """
    msgs = []
    while not send_ret_que.empty():
        msg = await send_ret_que.get()
        msgs.append(msg)
    gc.collect()
    yield from jsonify(response, {'results': msgs})


def get_espnow_peers():
    """
    Return dict with peers loaded from ESPNow
    """
    peers_b = [p[0] for p in espn.get_peers()]
    peers_s = []
    for p in peers_b:
        s = data.convert_mac_from_b(p)
        peers_s.append(s)
        if s not in stats:
            stats[s] = {}
    return {'peers': peers_s}


def get_peers_from_form(form):
    """
    Load peers sent via FORM into array
    """
    new_peers = []
    if 'peers' in form:
        if isinstance(form['peers'], list):
            new_peers = form['peers']
        else:
            new_peers = [form['peers']]
    for i in range(len(new_peers)):
        new_peers[i] = new_peers[i].strip("""'" """)

    return new_peers


async def set_time():
    """
    Set the time from NTP
    """
    while True:
        if is_connected():
            try:
                import ntptime
                if 'network' in config and 'ntp' in config['network']:
                    ntptime.host = config['network']['ntp']
                ntptime.settime()
            except Exception:
                # Ignore errors
                pass
        else:
            await asyncio.sleep(5)
            continue
        gc.collect()
        await asyncio.sleep(3600)


async def db_flush():
    """
    Flush data in DB to storage, keep returns queue short
    """
    while True:
        print('DF ', end='')
        while send_ret_que.qsize() > 128:
            await send_ret_que.get()
        flash_free = data.get_fs_stats()['free']
        if flash_free < 12288:
            biggest_grp = None
            chunks = 0
            for grp in fromesp_que:
                if fromesp_que[grp].status()['chunks'] > chunks:
                    chunks = fromesp_que[grp].status()['chunks']
                    biggest_grp = grp

            if biggest_grp:
                fromesp_que[biggest_grp].drop_chunk(0)
                print('\tLow free space, chunk dropped from grp ', biggest_grp)
            else:
                print('\tLow free space, but nothing to drop!')
        gc.collect()
        await asyncio.sleep(607)


async def esp_receiver():
    """
    Function receiving data from ESPNow network, decode them to Dict and
    save to datastore
    """
    from ujson import dumps
    s = asyncio.StreamReader(espn)
    while True:
        print('ER ', end='')
        response = await s.read(-1)
        for res in response.split(b'\x99')[1:]:
            if res and (len(res) >= res[0]+1+6):
                # len(res) == lenbyte + MAC + data
                res = res[1:res[0]+1+6]
                message = {}
                p = 'ID:'+res[6:].decode('utf-8')
                for m in p.split(';'):
                    if len(m) < 3:
                        continue
                    k, v = m.split(':', 1)
                    try:
                        v = int(v)
                    except ValueError:
                        pass
                    message[k] = v
                msg = {'from': data.convert_mac_from_b(res[:6]),
                       'message': message,
                       'time': time.time()+T_OFFSET}
                stats[msg['from']] = {'message': message,
                                      'time': time.time()+T_OFFSET}
                print('\tMSG ESPNOW R:', msg)
                # print(' R ({}B)'.format(len(res))
                # fromesp_que.push(msg)
                grp = grp_peers_map.get(res[:6], DEFAULT_GROUP)
                await fromesp_que[grp].put(dumps(msg))
            else:
                print("\tMSG ESPNOW R len error:", len(res), res[0], res)
        gc.collect()


async def esp_sender():
    """
    Send data/commands to ESPNow network, sending status put to queue
    Content of this queue is accessible via sendto/returns
    """
    while True:
        print('ES ', end='')
        peer, msg = await toesp_que.get()
        answer = {'peer': peer, 'time': time.time()+T_OFFSET}
        peer = data.convert_mac_to_b(peer)
        print('\tMSG ESPNOW S:', peer, msg)
        try:
            answer['sent'] = espn.send(peer, msg+'\n')
            if not answer['sent']:
                answer['error'] = 'Peer not available'
        except OSError as err:
            answer['sent'] = False
            answer['error'] = err

        await send_ret_que.put(answer)


def mk_grp_map(groups):
    grp_map = {}

    for (k, v) in groups.items():
        for i in v:
            grp_map[data.convert_mac_to_b(i)] = k

    return grp_map


def fromesp_que_status():
    stat = {}
    for grp in fromesp_que:
        stat[grp] = fromesp_que[grp].status()
    return stat


def main():
    """
    Set up the tasks and start the event loop
    """
    global grp_peers_map

    fromesp_que[DEFAULT_GROUP] = data.SavedQueue('/data/{}/'.format(DEFAULT_GROUP))
    for group in config.get('groups', {}):
        fromesp_que[group] = data.SavedQueue('/data/{}/'.format(group))
        for peer in group:
            stats[peer] = {}

    grp_peers_map = mk_grp_map(config.get('groups', {}))

    connect()
    espn.init()
    for peer in config.get('peers', []):
        stats[peer] = {}
        peer = data.convert_mac_to_b(peer)
        espn.add_peer(peer, ifidx=AP_IF)

    loop = asyncio.get_event_loop()
    loop.create_task(set_time())
    loop.create_task(db_flush())
    loop.create_task(esp_receiver())
    loop.create_task(esp_sender())
    loop.create_task(asyncio.start_server(webapp.handle, '0.0.0.0', 80))
    gc.collect()
    loop.run_forever()
