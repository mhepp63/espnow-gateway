import gc
import json
from ustruct import unpack

import uerrno as errno
import uasyncio as asyncio


# Exception raised by get_nowait().
class QueueEmpty(Exception):
    pass


# Exception raised by put_nowait().
class QueueFull(Exception):
    pass


def convert_mac_to_b(mac):
    return bytes([int(x, 16) for x in mac.split(':')])


def convert_mac_from_b(mac):
    p = unpack('6B', mac)
    return '{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}'.format(p[0], p[1], p[2],
                                                              p[3], p[4], p[5])


def get_fs_stats():
    from os import statvfs
    stat = statvfs('/')
    data = {'size': stat[2]*stat[1],
            'free': stat[3]*stat[0]}
    gc.collect()
    return data


class Config(dict):
    """
        Dict with autosave capability
    """
    def __init__(self, config_file='/data/config.json', default=None):
        super().__init__()
        if default is not None:
            for key in default:
                super().__setitem__(key, default[key])
        self.c_file = config_file
        self.load()

    def __del__(self):
        self.save()
        super().__del__()

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.save()

    def __delitem__(self, key):
        super().__delitem__(key)
        self.save()

    def update(self, *args, **kwargs):
        r = super().update(*args, **kwargs)
        self.save()
        return r

    def pop(self, *args):
        r = super().pop(*args)
        self.save()
        return r

    def load(self):
        try:
            with open(self.c_file, 'r') as f:
                d = json.load(f)
                for k in d:
                    super().__setitem__(k, d[k])
        except OSError as err:
            print('Config load Exception:', err)
            raise(err)

    def load_key_from_file(self, key, fname):
        try:
            with open(fname, 'r') as f:
                val = json.load(f)
                super().__setitem__(key, val)
        except OSError as err:
            print('Config load Exception:', err)
            raise(err)

    def save(self):
        try:
            with open(self.c_file, 'w') as f:
                json.dump(self, f)
        except OSError as err:
            print('Config save Exception:', err)
            raise(err)


class SavedQueue():

    def __init__(self, data_dir, chunk_size=256, queue_size=16):
        """
        Init class, loads files with chunks saved in directory 'data_dir'
            data_dir: directory for data storage, each file contains received
                data stored when len(queue) >= queue_size while number of line
                in chunk file less then chunk_size
            chunk_size: number of messages in chunk file
            queue_size: Size of internal queue. When longer, then flushed

        Because of resource saving, class expect text-based items in queue.
        If you need to store something else, you must convert it to single line
        str before putting to queue (this means no '\n' inside)!
        """
        # chunk_size how many lines in chunk file
        # queue_size how many items in queue
        self.data_dir = data_dir
        self.chunk_size = chunk_size
        self.queue_size = queue_size
        self._queue = []
        self._load_chunks()
        self._open_chunk = None
        self._msgs_r = 0
        self._msgs_w = 0
        self._evput = asyncio.Event()  # Triggered by put, tested by get
        self._evget = asyncio.Event()  # Triggered by get, tested by put
        # self._lockflush = asyncio.Lock()
        print("Loaded chunks {} from dir {}.".format(self._chunks,
                                                     self.data_dir))

    def __del__(self):
        self._flush_queue()

    def _load_chunks(self):
        """
        Load data chunks in dir - each chunk must have name as
        hexadecimal number. All chunks are sorted incrementally in _chunks list
        """
        from os import ilistdir, remove

        self._chunks = []
        try:
            for chunk in ilistdir(self.data_dir):
                if chunk[1] == 0x8000 and chunk[3] > 0:
                    try:
                        # chunk name is a hexadecimal number
                        int(chunk[0], 16)
                    except ValueError:
                        continue
                    # if chunk is file with non-zero size, append it
                    self._chunks.append(int(chunk[0], 16))
                if chunk[1] == 0x8000 and chunk[3] <= 0:
                    # chunk is file with zero/unknown size, delete it
                    remove(self.data_dir+'/'+chunk[0])
                else:
                    # some other stuff...
                    continue
        except OSError as err:
            if err.value == errno.ENOENT:
                self._mk_datadir()
            else:
                raise(err)
        self._chunks.sort()

    def _get_next_chunk(self):
        """
        Returns ID of next chunk - used when flushing queue to file
        """
        if len(self._chunks) > 0:
            self._next_chunk = self._chunks[-1] + 1
        else:
            self._next_chunk = 1
        return self._next_chunk

    def _mk_datadir(self):
        """
        Prepare path for storing chunks if does not exists
        """
        from os import mkdir

        path = ''
        for part in self.data_dir.split('/'):
            if len(part):
                path = path + '/' + part
                try:
                    mkdir(path)
                except OSError as err:
                    if err.errno == errno.EEXIST:
                        continue
                    else:
                        raise(err)

    def _mk_chunk_path(self, ch_id):
        """
        Return full name with path of chunk file
        """
        return self.data_dir+'/{:08x}'.format(ch_id)

    def _get_from_chunk(self):
        """
        Return first line from chunk file

        Helper function for self._get()
        """
        data = None
        if not self.empty_chunks():
            fname = self._mk_chunk_path(self._chunks[0])
            with open(fname, 'r') as fp:
                lines = fp.readlines()
                data = lines.pop(0)
            with open(fname, 'w') as fp:
                for line in lines:
                    fp.write(line)
        gc.collect()
        return data

    def _get(self):
        """
        Get first stored item - if no chunk files, then from _queue, else from
        first chunk file
        """
        data = None
        self._evget.set()  # Schedule all tasks waiting on get
        self._evget.clear()
        if not self.empty_chunks():
            data = self._get_from_chunk()
        else:
            data = self._queue.pop(0)
        self._msgs_r = self._msgs_r+1
        return data

    async def get(self):  # Usage: item = await queue.get()
        """
        Async variant of get - get first stored item more in _get

        DO NOT use often, read whole chunk file to list, pop first item and
        write rest back - huge writing to flash!
        """
        while self.empty():  # May be multiple tasks waiting on get()
            # Queue is empty, suspend task until a put occurs
            # 1st of N tasks gets, the rest loop again
            await self._evput.wait()
        return self._get()

    def get_nowait(self):  # Remove and return an item from the queue.
        """
        Return an item if one is immediately available, else raise QueueEmpty

        DO NOT use often, read whole chunk file to list, pop first item and
        write rest back - huge writing to flash!
        """
        if self.empty():
            raise QueueEmpty()
        return self._get()

    async def get_all(self):
        """
        Yields all stored items at once:
            from chunk files first, then from queue
        """
        from os import remove
        # while self.empty():
        #     await self._evput.wait()

        while len(self._chunks) > 0:
            chunk = self._chunks.pop(0)
            fname = self._mk_chunk_path(chunk)
            with open(fname, 'r') as fp:
                for line in fp.readlines():
                    self._msgs_r = self._msgs_r+1
                    yield line.rstrip('\n')
                fp.flush()
            remove(fname)
        self._open_chunk = None

        while len(self._queue) > 0:
            self._msgs_r = self._msgs_r+1
            line = self._queue.pop(0)
            yield line
        gc.collect()

    def _flush_queue(self):
        """
        Flush queue to a new chunk file
        """
        mode = 'a'
        # self._lockflush.acquire()
        if self._open_chunk is None:
            ch_id = self._get_next_chunk()
            self._chunks.append(ch_id)
            self._open_chunk = [ch_id, 0]
            mode = 'w'

        fname = self._mk_chunk_path(self._open_chunk[0])
        with open(fname, mode) as fp:
            while len(self._queue) > 0:
                item = self._queue.pop(0)
                fp.write(item)
                fp.write('\n')
                self._open_chunk[1] = self._open_chunk[1] + 1
                if self._open_chunk[1] >= self.chunk_size:
                    self._open_chunk = None
                    break
        gc.collect()

    def _put(self, val):
        """
        Helper function for putting data into queue
        """
        self._evput.set()  # Schedule tasks waiting on put
        self._evput.clear()
        self._queue.append(val)
        self._msgs_w = self._msgs_w + 1
        if len(self._queue) >= self.queue_size:
            # while self._lockflush.locked():
            #     await asyncio.sleep_ms(10)
            self._flush_queue()

    async def put(self, val):  # Usage: await queue.put(item)
        while self.full():
            # Queue full
            await self._evget.wait()
            # Task(s) waiting to get from queue, schedule first Task
        self._put(val)

    def put_nowait(self, val):  # Put an item into the queue without blocking.
        if self.full():
            raise QueueFull()
        self._put(val)

    def qsize(self):  # Number of items in the queue.
        return len(self._queue)

    def empty_chunks(self):
        return len(self._chunks) == 0

    def empty_list(self):
        return len(self._queue) == 0

    def empty(self):  # Return True if the queue is empty, False otherwise.
        return self.empty_list() and self.empty_chunks()

    def full(self):  # Return True if there are queue_size items in the queue.
        # Note: if the Queue was initialized with queue_size=0 (the default) or
        # any negative number, then full() is never True.
        return self.queue_size > 0 and self.qsize() >= self.queue_size

    def drop_chunk(self, chunk_index):
        from os import remove
        try:
            chunk = self._chunks.pop(chunk_index)
        except IndexError:
            return
        chunk = self._mk_chunk_path(chunk)
        remove(chunk)

    def status(self):
        ch_name = self._open_chunk[0] if self._open_chunk else -1
        ch_writ = self._open_chunk[1] if self._open_chunk else -1
        return {'que_len': self.qsize(),
                'chunks': self._chunks,
                'messages': {'received': self._msgs_w,
                             'sent': self._msgs_r},
                'open_chunk': {'name': ch_name,
                               'written': ch_writ}}
