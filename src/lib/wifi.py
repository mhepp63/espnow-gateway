from network import WLAN, STA_IF, AP_IF
from lib import data


def is_connected():
    """
    Check if the WLAN is connected to a network
    """
    wlan = WLAN(STA_IF)
    return wlan.active() and wlan.isconnected()


def connect():
    """
    Connect to the WiFi network based on the configuration.
    Fails silently if there is no configuration.
    """
    cfg = data.Config('/data/config.json')
    try:
        wlan0 = WLAN(STA_IF)
        if wlan0.active() and wlan0.isconnected():
            pass
        else:
            wlan0.active(True)
            wlan0.connect(cfg['network']['essid'],
                          cfg['network']['password'])
    except Exception as e:
        # Just fail silently for now
        print(e)

    try:
        wlan1 = WLAN(AP_IF)
        if not wlan1.active():
            wlan1.active(True)
        wlan1.config(hidden=True)
    except Exception as e:
        # Just fail silently for now
        print(e)

    print(wlan0.ifconfig())


def get_ip():
    """
    Get the IP address for the current active WLAN
    """
    wlan = WLAN(STA_IF)
    if wlan.active() and wlan.isconnected():
        details = wlan.ifconfig()
        details = details if details else [None, None, None, None]
        ip_address = {'ip': details[0],
                      'netmask': details[1],
                      'gateway': details[2],
                      'nameserver': details[3]}
    return ip_address
