from lib.app import main
from lib.data import Config

cfg = Config('/data/config.json')

if 'network' not in cfg:
    cfg.load_key_from_file('network', '/network.json')
    del cfg

try:
    main()
except Exception as e:
    print("Exception:", e)
